<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cleanseas');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sq+T[)sURn(evc3G/b+vtzo9|*ID^D0WL?u)uZtBP:q}zkufW(V egmHrCEu35%1');
define('SECURE_AUTH_KEY',  '2};FVJUz9rF;CfyBf44S|In%5r^;c#EWR%XEACD{2TpkZ?[Uqr7V{xhudFfu}>kc');
define('LOGGED_IN_KEY',    'B0~YQ1`&eH6~8C4)o)>X^i6XVmH{eIw<XW@wNB-X,VRgBfV{<gGUVuTJf`.6!dTA');
define('NONCE_KEY',        '%_U|+ujVT*i&>pe1.H!1En3P-IsncG-JC^2.PN9W{om yTF+_ZFCh4x^]T;I3/-@');
define('AUTH_SALT',        '~#n$r.<>m*c{M>ySX8cPZ5q7i72`y<>o}Eu@fq~BI2:=>v_9 +7|2Whrns7$~R9r');
define('SECURE_AUTH_SALT', ';w@N=rF>VevYAxXf`?6Aw0D<nFwoQ#V$1g(lPv58%jF4o}KlY.$>_K7>Vq_z2_S=');
define('LOGGED_IN_SALT',   '{-H?6IrkX?n<f$.hadJt;i~jsHYfi_>-tjJL[d>nh8N*~9W+?yLM#~ ot7<$6![Y');
define('NONCE_SALT',       '2j~~KCuEtTPJ@@*2:g<)H~EXf)lC+ng^~oXK8O3P5~9(=j^m?eyK{Eb^=ce]FePA');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

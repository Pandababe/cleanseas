<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cleanseas</title>
    <?php wp_head(); ?>
    <link href="https://fonts.googleapis.com/css?family=Hind:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/custom.css">
</head>
<body>
    <header>
        <nav class="navbar">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo" href="#">
                        <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/images/logo.png" alt="">
                    </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Photos</a></li>
                        <li><a href="#">News</a></li>
                        <li><a href="#">Resources</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header>
    <main>
        <section class="sectionarea-banner">
            <div class="banner-bg" style="background: url('<?php echo get_template_directory_uri();?>/images/bg.jpg')">
            </div>
            <div class="diagonal-01"></div>
        </section>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 category-section">
                        <h2 class="head">Choose your Category</h2>
                        <p class="text"> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type s</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                            <div class="row form-container">
                                <div class="form-item">
                                    <p class="forms"><a href="">Individual<span>que porro quisquam est q</span></a></p>
                                </div>
                                <div class="form-item">
                                    <p class="forms"><a href="">Private<span>que porro quisquam est q</span></a></p>
                                </div>
                                <div class="form-item">
                                    <p class="forms"><a href="">Public<span>que porro quisquam est q</span></a></p>
                                </div>
                                <div class="form-item">
                                    <p class="forms"><a href="">Group<span>que porro quisquam est q</span></a></p>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-6 pledge-container">
                            <h3 class="head">Help us turn the tide?</h3>
                            <h1 class="head">PLEDGE</h1>
                            <h3 class="head">#BEAT<span>PLASTIC</span>POLLUTION</h3>
                            <p class="button"><a href="">Start Now</a></p>
                            <p class="text">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                    </div>
                </div>
            </div>
            
        </section>
        <section class="view">
                <div class="banner-bg first" style="background: url('<?php echo get_template_directory_uri();?>/images/bg.jpg')">
                    <div class="container">
                        <div class="row center-content-y">
                            <div class="col-md-8">
                                <p>
                                    The risk of your health being negatively <br>
                                    impacted by polluted drinking water in <br>
                                    a developed country is small in comparison <br>
                                    with developing countries <br>
                                    However it is possible to become <br>
                                    ill from containing
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banner-bg second" style="background: url('<?php echo get_template_directory_uri();?>/images/bg.jpg')">
                    <div class="container">
                        <div class="row center-content-y">
                            <div class="col-md-8">
                                <p>
                                    The risk of your health being negatively <br>
                                    impacted by polluted drinking water in <br>
                                    a developed country is small in comparison <br>
                                    with developing countries <br>
                                    However it is possible to become <br>
                                    ill from containing
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banner-bg third" style="background: url('<?php echo get_template_directory_uri();?>/images/bg.jpg')">
                    <div class="container">
                        <div class="row center-content-y">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <p>
                                    The risk of your health being negatively <br>
                                    impacted by polluted drinking water in <br>
                                    a developed country is small in comparison <br>
                                    with developing countries <br>
                                    However it is possible to become <br>
                                    ill from containing
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <section class="border-blue">
            <div class="banner-bg" style="background: url('<?php echo get_template_directory_uri();?>/images/bg.jpg')">
                <div class="row center-content-yx">
                    <h2>How you can help ?</h2>
                </div>
            </div>
        </section>
        <section>
            <div class="take-pledge">
                <h2>TAKE THE PLEDGE</h2>
            </div>
        </section>
    </main>
    <footer>
        <div class="row">
            <div class="col-sm-2">
                <h3>Head</h3>
                <p class="text">Testing is a good choice</p>
            </div>
            <div class="col-sm-5">
                <h3>Head</h3>
                <p class="text">Testing is a good choice Testing is a good choice Testing is a good choice Testing is a good choice Testing is a good choice</p>
            </div>
            <div class="col-sm-3">
                <ul class="social-icons">
                    <li><a href=""><img class="img-responsive" src="<?php echo get_template_directory_uri();?>/images/fb.png" alt="" srcset=""></a></li>
                    <li><a href=""><img class="img-responsive" src="<?php echo get_template_directory_uri();?>/images/tw.png" alt="" srcset=""></a></li>
                    <li><a href=""><img class="img-responsive" src="<?php echo get_template_directory_uri();?>/images/ig.png" alt="" srcset=""></a></li>
                </ul>
            </div>
            <div class="col-sm-2 last-img">
                <p><img class="img-responsive" src="<?php echo get_template_directory_uri();?>/images/logo.png" alt=""></p>
                <p><img class="img-responsive" src="<?php echo get_template_directory_uri();?>/images/footer-logo.png" alt=""></p>
            </div>
        </div>
    </footer>


</body>
<?php wp_footer(); ?>
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/custom.js"></script>
</html>